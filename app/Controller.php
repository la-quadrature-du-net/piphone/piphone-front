<?php

class Controller {
	function __construct() {
		$f3=Base::instance();
    }

    // Function called before routing
    function beforeRoute($f3, $args) {
        // Include configuration
        require_once('config.php');

        // Languages available
        $languages = array(
            array("iso" => "fr", "name"=>"Français"),
            array("iso" => "en", "name"=>"English"),
        );
        $f3->set('languages', $languages);

        // Init campaign informations
        $f3->set('campaign', Api::get_campaign());

        // Get organization informations
        $f3->set('organization', Api::get_organization());
        // Init contacts list if needed
        if (!$f3->exists('contacts')) {
            $f3->set('contacts', Api::get_contacts(), API_TIMEOUT);
        }
        // Workaround for caching variables
        $f3->set('contacts', $f3->get('contacts'));

        $groups = Api::get_groups();
        $f3->set('groups', $groups);

        $group_types = Api::get_group_types();
        $f3->set('group_types', $group_types);
    }

    // Function called after routing
    function afterRoute($f3, $args) {
		// Rendu HTML de la page
		echo Template::instance()->render('base.html');
    }

};

