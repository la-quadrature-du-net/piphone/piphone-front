<?php

use GuzzleHttp\Client;

/*
 * Static class for REST API communication
 */
class Api {
    // Generic get function for REST API
    static function get($url) {
        $client = new Client(['base_uri' => API_BASE, 'defaults' => [ 'headers' => [ 'Content-type' => 'applications/json']]]);
        $url = API_BASE . $url . "/?format=json";

        $result = $client->request('GET', $url);
        return $result->getBody();
    }

    // Generic post function for REST API
    static function post($url, $data) {
        try {
            $client = new Client(['base_uri' => API_BASE, 'defaults' => [ 'headers' => [ 'Content-type' => 'applications/json']]]);
            $url = API_BASE . $url . "/?format=json";
            $result = $client->request('POST', $url, ['body' => json_encode($data), "headers" => ['Content-Type' => 'application/json']]);
            return $result->getBody();
        } catch (Exception $e) {
            echo $e;
            if ( $e->hasResponse()) {
                echo $e->getResponse()->getBody();
            } else {
                echo $e;
            }
        }
    }

    // Asking for campaign informations
    static function get_campaign() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID);
        return json_decode($json, true);
    }

    // Asking for complete list of contacts
    static function get_contacts() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/contacts");
        return json_decode($json, true);
    }

    // Asking for a single contact
    static function get_contact($id) {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/contacts/" . $id);
        return json_decode($json, true);
    }

    // Asking for complete list of groups
    static function get_groups() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/groups");
        return json_decode($json, true);
    }

    // //Asking for complete list of group types
    static function get_group_types() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/grouptypes");
        return json_decode($json, true);
    }

    // Asking for complete list of feedback categories
    static function get_feedback_categories() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/categories");
        return json_decode($json, true);
    }

    // Asking for arguments by language
    static function get_arguments($language='en') {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/arguments");
        return json_decode($json, true);
    }

    // Asking for organization details
    static function get_organization() {
        $json = Api::get("campaigns/" . CAMPAIGN_ID . "/organization");
        return json_decode($json, true);
    }

    // Posting a feedback
    static function post_feedback($contact_id, $comment, $feedback_category='') {
        $data = array(
            "callee"=>$contact_id,
            "comment"=>$comment,
            "category"=>$feedback_category
        );
        $json = Api::post("campaigns/" . CAMPAIGN_ID . "/feedbacks", $data);
        return json_decode($json, true);
    }
};

