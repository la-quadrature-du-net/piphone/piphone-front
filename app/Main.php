<?php

use \Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class Main extends Controller {
    /*
     * Home page
     * Presents campaign information and a random Contact
     */
    function home($f3,$args) {
        // Select a random contact
        //TODO: use weight for random
        $contacts = $f3->get('contacts');

        $groups = Api::get_groups();
        $f3->set('groups', $groups);

        $f3->set('block_content', 'home.html');
    }

    /*
     * Feedback page
     * Form for a feedback after a call
     * GET: show the form
     * POST: send the form to the campaign API and show thank you
     */
    function feedbackform($f3, $args) {
        //GET
        if ($f3->get('VERB') == 'GET'){
            $categories = Api::get_feedback_categories();
            $f3->set('feedback_categories', $categories);

            $contact_id = $f3->get('POST.contact_id');
            $f3->set("contact_id", $contact_id);

            $arguments = Api::get_arguments();
            $f3->set('arguments', $arguments);

            $f3->set('block_content', 'feedbackform.html');
        }
        //POST
        elseif ($f3->get('VERB') == 'POST'){
            $contact_id = $f3->get('POST.contact_id');
            $feedback = $f3->get('POST.feedback');
            $category = $f3->get('POST.feedback_category');

            //send feedback to campaign
            $f3->set('post_feedback_result', Api::post_feedback($contact_id, $feedback, $category));
            $f3->set('block_content', 'thankyou.html');
        }
    }

    /*
     * call Page
     * Form to call
     * GET: show the form
     * POST: send the form and GET feedbackform
     */
    function call($f3, $args) {
        //GET
        if ($f3->get('VERB') == 'GET') {
            $categories = Api::get_feedback_categories();
            $f3->set('feedback_categories', $categories);
            $f3->set('contact_id', $args['id']);
            $f3->set('block_content', 'feedbackform.html');
            $f3->set("contact_id", $contact_id);
            $arguments = Api::get_arguments();
            $f3->set('arguments', $arguments);
            $f3->set('block_content', 'feedbackform.html');
        }
        //POST
        elseif ($f3->get('VERB') == 'POST'){
            // To get the callee, we have the callee_id in the form. Using that
            // to load the callee and retrieve its number.
            $contact = Api::get_contact($args['contact_id']);
            $f3->set('contact', $contact);
            $call_id = str_replace('.', '-', uniqid('', true));
        }
    }

    function contactslist($f3, $args) {
        $contacts = Api::get_contacts();
        $f3->set('contacts', $contacts);

        $f3->set('block_content', 'contactslist.html');
    }

    function campaign($f3, $args) {
        $arguments = Api::get_arguments();
        $f3->set('arguments', $arguments);

        $f3->set('block_content', 'campaign.html');
    }

    function piphone($f3, $args) {
        $f3->set('block_content', 'piphone.html');
    }
};
;
