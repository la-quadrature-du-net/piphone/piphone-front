<?php

use \Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class SIP extends Controller {
    function beforeRoute($f3, $args) {
        // Include configuration
        require_once('config.php');
    }

    function afterRoute($f3, $args) {
        // We return only JSON, so no templates
        echo $f3->get('response');
    }
    // Getting a call from the SIP_API
    function feedbacksip($f3, $args) {
        $token = JWT::encode(array('api' => JWT_TOKEN), JWT_KEY);
        $call_id = $args['callid'];
        $data = array('api' => JWT_TOKEN,
            'token' => $token
        );
        $client = new GuzzleHttp\Client(['base_uri' => SIP_API]);
        try {
            $token = JWT::encode(array('api' => JWT_TOKEN, 'nbf' => time() - 5, 'exp' => time() + 10*60, 'iat' => time()), JWT_KEY);
            $res = $client->get("calls/$call_id", ['query' => $data]);
            $f3->set('response', (GuzzleHttp\Psr7\copy_to_string($res->getBody())));
        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                $f3->set('response', Psr7\str($e->getResponse()));
            }
            $f3->set('response', Psr7\str($e->getRequest()));
        }
    }
}
