<?php
require_once("vendor/autoload.php");

// Retrieve instance of the framework
$f3 = Base::instance();

// Initialize CMS
$f3->config('app/config.ini');

// Define routes
$f3->config('app/routes.ini');

// Execute application
$f3->run();
