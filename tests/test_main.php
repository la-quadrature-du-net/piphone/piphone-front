<?php
/*
 * Tests for Main class of piphone
 */
include('BaseTest.php');

class Test_Main_Class extends BaseTest {
    function setup() {
        parent::setup();
        $this->f3->set('QUIET',TRUE);
    }

    function teardown() {
        parent::teardown();
        $this->f3->set('QUIET',FALSE);
    }

    function test_main_existence() {
        $this->test->expect(
            class_exists('Main'),
            'Main class is not defined'
        );
    }

    function test_main_get_campaign_return_array() {
        $this->f3->mock('GET /');
        $this->test->expect(
            is_array($this->f3->get('campaign')),
            'Do not get a campaign array'
        );
        $this->f3->clear('ERROR');  // clear any errors
    }

    function test_main_get_campaign_contains_known_fields() {
        $fields = array("title", "description", "start_date", "end_date", "default_language");
        $this->f3->mock('GET /');
        foreach ($fields as $field) {
            $this->test->expect(
                array_key_exists($field, $this->f3->get('campaign')),
                "Campaign should have a " . $field ." field"
            );
        }
        $this->f3->clear('ERROR');  // clear any errors
    }

    function test_main_get_feedbackform_return_categories_array() {
        $this->f3->mock('GET /feedback/');
        $this->test->expect(
            is_array($this->f3->get('feedback_categories')),
            "Do not get a categories array"
        );
        $this->f3->clear('ERROR');  // clear any errors
    }

    function test_main_get_feedbackform_return_contact_id() {
        $this->f3->mock('GET /feedback/');
        $this->test->expect(
            !is_nan($this->f3->get('contact_id')),
            "Do not get a numeric contact id"
        );
        $this->f3->clear('ERROR');  // clear any errors
    }
}

// Launching tests
$tests = new Test_Main_Class();
$tests->run();
