<?php
/*
 * Launching tests for each test file
 */
$directory = opendir('.');
$file = readdir($directory);
while ($file) {
    if (substr($file,0,4)=="test") {
        include($file);
    }
    $file = readdir();
}
