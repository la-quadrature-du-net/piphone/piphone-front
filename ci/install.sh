#!/bin/bash
# This schript is used to setup what needs to be (virtualenv and stuff)
# before running the pthon setup.py part
# We need those variables
if [ -z "$CI_PROJECT_DIR" ]
then
	echo "CI_PROJECT_DIR undefined"
	exit 1
fi

export CI_PROJECT_NAME=$(basename $CI_PROJECT_DIR)

if [ -z "$CI_REPOSITORY_URL" ]
then
	echo "CI_REPOSITORY_URL undefined"
	exit 1
fi

cd /srv/piphone
if [ -d $CI_PROJECT_NAME ]
then
	cd $CI_PROJECT_NAME
	git pull origin master
else
	git clone $CI_REPOSITORY_URL $CI_PROJECT_NAME
	cd $CI_PROJECT_NAME
fi

composer install || composer update
