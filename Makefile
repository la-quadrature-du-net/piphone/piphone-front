.PHONY: help doctor install reset-db translations
.DEFAULT_GOAL := help

help:
	@echo "\033[33mUsage:\033[0m"
	@echo "  make [command]"
	@echo ""
	@echo "\033[33mAvailable commands:\033[0m"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' Makefile | sort \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "  \033[32m%s\033[0m___%s\n", $$1, $$2}' | column -ts___

doctor: ## Check that everything is installed to use this application
	@echo "\033[1m\033[36m==> Check required dependencies\033[0m\033[21m"
	@which composer >/dev/null 2>&1 && echo "\033[32mcomposer installed\033[0m" || echo "\033[31mcomposer not installed\033[0m"
	@echo "\033[1m\033[36m==> Check configuration\033[0m\033[21m"
	@test -s ./app/config.php && echo "\033[32mConfiguration OK\033[0m" || echo "\033[31mYou need to copy app/config.php.sample to app/config.php in order to configure your application.\033[0m"
	@echo "\033[1m\033[36m==> Check optional dependencies\033[0m\033[21m"
	@which msgmerge >/dev/null 2>&1 && echo "\033[32mmsgmerge installed\033[0m" || echo "\033[31mmsgmerge not installed\033[0m"
	@which msgfmt >/dev/null 2>&1 && echo "\033[32mmsgfmt installed\033[0m" || echo "\033[31mmsgfmt not installed\033[0m"
	@which xgettext >/dev/null 2>&1 && echo "\033[32mxgettext installed\033[0m" || echo "\033[31mxgettext not installed\033[0m"

install: ## Install the application
	@echo "\033[1m\033[36m==> Install Composer dependencies\033[0m\033[21m"
	@composer install

server-start: server-stop ## Launch a local server
	@php -S 127.0.0.1:8000 >> .server.log &
	@echo "\033[32mServer running. (http://127.0.0.1:8000)\033[0m"

server-stop: ## Stop local server if running
	@ps -aux | grep "[p]hp -S 127.0.0.1:8000" | grep -v grep | awk '{print $$2}' | xargs -r -n 1 kill
	@echo "\033[32mServer stopped. (http://127.0.0.1:8000)\033[0m"

translations: locales/fr_FR/LC_MESSAGES/messages.mo locales/en_US/LC_MESSAGES/messages.mo ## Generate translations

messages.pot: app/*.php templates/*.html
	[ -r $@ ] || touch $@
	xgettext --package-name=LQDNCampaign --package-version=2016.1 --force-po -o $@ --keyword=__ --keyword=_  --from-code=UTF-8 $^

locales/%/LC_MESSAGES/messages.po: messages.pot
	msgmerge -v -U $@ $^

locales/fr_FR/LC_MESSAGES/messages.mo: locales/fr_FR/LC_MESSAGES/messages.po
	msgfmt $^ -o $@

locales/en_US/LC_MESSAGES/messages.mo: locales/en_US/LC_MESSAGES/messages.po
	msgfmt $^ -o $@
